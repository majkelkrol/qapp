import SwiftUI
import WebKit


struct ContentWebView2: View {
    var body: some View {
        SwiftUIWebView2(url: URL(string: "https://www.o2.pl/quiz/kobieta/6792881881659009/start"))
            .navigationTitle("🧸🤔")
    }
}

struct ContentWebView2_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ContentView()
        }
    }
}

struct SwiftUIWebView2: UIViewRepresentable {
    
    let url: URL?
    
    func makeUIView(context: Context) -> WKWebView {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true
        let config = WKWebViewConfiguration()
        config.defaultWebpagePreferences = prefs
        return WKWebView(frame: .zero, configuration: config)
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        guard let myURL = url else {
            return
        }
        let request = URLRequest(url: myURL)
        uiView.load(request)
    }
}

