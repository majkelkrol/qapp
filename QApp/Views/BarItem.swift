import SwiftUI

struct BarItem: View {
    var body: some View {
        TabView {
            WebView()
                .tabItem {
                    Image(systemName: "house")
                    Text("Home")
                }
            Text("Settings")
                .tabItem {
                    Image(systemName: "gear")
                    Text("Settings")
                }
        }
    }
}

struct BarItem_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            BarItem()
        }
    }
}
